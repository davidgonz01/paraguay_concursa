Proyecto hackathon - Equipo DAARTIC

Asistente Virtual del Gobierno
Habilidades 
    1 Responder a consultas de Oportunidades Laborales del Estado
    2 Responder a consultas sobre horarios de Atencion en el Instituto de Prevision Social
    
    se adjunta los archivos
     Lineamiento de Proyecto - Plantilla (2).docx
    skill-concursa-aplicacion.json 
    skill-Consulta_IPS.json
   
    
los archivos con extension .json son archivos que contiene la base y los datos a ser mostrados en cada interaccion 
el mismo puede subirse en la plataforma de IBM y ejecutarse online o OFFLINE via API
https://cloud.ibm.com/ 
    
     contiene las entidades, intenciones de la consultas y los dialogos construidos para dar Respuestas segun el contexto.
    
    adicionalmente se adjunta los videos del funcionamiento del asistente para verificar el funcionamiento.
    
    
    Los datos son utilizados son de los sitios
        https://www.paraguayconcursa.gov.py/sicca/Portal.seam?logic=and&cid=956561 
        http://servicios.ips.gov.py/IPSMaps/buscamed3.php
    
    el asistente puede implementarse dentro de las paginas oficiales del estado sin mayores requerimientos
    o integrarse via Facebook Messenger a alguna cuenta oficial de la institucion
    
    cualquier consulta dirigirse al correo david.gonz.01@gmail.com (lider del equipo)
